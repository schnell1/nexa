import os
from pathlib import Path
import re
import jinja2
from fluent_compiler.bundle import FluentBundle
from markdown import markdown
from markdown.extensions.toc import TocExtension
from shutil import rmtree

def exec(cmd: str):
    return {"cmd": cmd, "exit_status": os.system(cmd)}

def rmdir(path):
    try:
        rmtree(path)
    except:
        pass

def mkdir(path):
    if isinstance(path, str):
        Path(path).mkdir(parents=True, exist_ok=True)
    if isinstance(path, Path):
        path.mkdir(parents=True, exist_ok=True)

def is_index(path):
    return path == ["/"] or path == "/" or path == "index"

def extract_frontmatter(text: str, tag = "xxx"):
    expr = '^' + tag + '[\s\S]+?' + tag
    frontmatter = re.findall(expr, text)
    if frontmatter:
        frontmatter_len = len(frontmatter[0])
        frontmatter = frontmatter[0][len(tag) + 1:-len(tag) + -1]
        body = text[frontmatter_len + 1:].lstrip()
        return frontmatter, body
    else:
        return "", text

def parse_bool(val):
    if isinstance(val, str):
        return val.lower() == "true" or val.lower() == "yes" or val == "1"
    if isinstance(val, bool):
        return val
    return False

class Page:
    def __init__(self, path, bundles: set({FluentBundle}), bodies: {str: str}):
        self.bodies = bodies
        self.path = path
        self.bundles = bundles
        self.default_locale = None
        self.assert_set_default_locale()
    
    def assert_set_default_locale(self):
        claimers = self.get_all("default", {})
        claimer = None
        for locale in claimers:
            is_claimer = parse_bool(claimers[locale])
            if is_claimer:
                if claimer:
                    raise Exception("\"default\" can only be set once for a translated set of a page.")
                else:
                    claimer = locale
        if not self.default_locale:
            self.default_locale = claimer
        return self.default_locale
    
    def is_published(self, locale, default):
        published = self.get("published", locale, {})
        if published == None:
            return default
        return parse_bool(published)
    
    def ctx(self, site, section, locale: str, args: {}, require_published, default_template: str):
        ctx = {"page": {}}

        default = False
        if locale == self.default_locale:
            default = True
        
        body = None
        if locale in self.bodies:
            body = self.bodies[locale]
        
        published = parse_bool(self.is_published(locale, default = False))
        ctx["page"].update({
            "path": self.path,
            "slug": self.slug(locale),
            "locale": locale,
            "default": default,
            "title": self.get("title", locale, {}),
            "href": self.href(section, locale),
            "body": body,
            "published": published,
            "template": self.template(locale, default_template),
        })

        if not published and require_published:
            return None
        
        elif self.has_locale(locale):
            bundle = self.bundle(locale)
            for key in bundle._compiled_messages:
                if key not in ctx["page"].keys():
                    msg, errs = bundle.format(key, args)
                    if msg:
                        ctx["page"].update({key: msg})

        return ctx
    
    def template(self, locale, default):
        t = self.get("template", locale, {})
        if t:
            return t
        else:
            return default
    
    def body(self, locale):
        if locale in self.bodies:
            return self.bodies[locale]
        return None

    def bundle(self, locale):
        for b in self.bundles:
            if b.locale == locale:
                return b
        return None

    def href(self, section, locale):
        slug = self.slug(locale)
        href = "/"

        if slug:
            href += slug
            return href
        
        if is_index(self.path):
            if locale != self.default_locale:
                href += locale
            return href
        
        section_slug = None
        if isinstance(section, Section):
            section_slug = section.page.slug(locale)
            if section_slug:
                href = section_slug
            else:
                href += str(Path(locale).joinpath("".join(section.path).lstrip("/")))
            if section.page == self:
                return href

        page_path = self.path
        page_path_end = page_path[len(page_path)-1:][0]
        href = Path(href).joinpath(page_path_end)
        
        return str(href)

    def add_bundles(self, bundles: set({FluentBundle})):
        # TODO: Check [bundle.locale in bundles] and maybe update [bundle in self.bundles], instead of calling self.bundles.update().
        self.bundles.update(bundles)
        self.assert_set_default_locale()
    
    def add_bodies(self, bodies: {str: str}):
        # TODO: Check [body.locale in bodies] and maybe update [body in self.bodies], instead of calling self.bodies.update().
        self.bodies.update(bodies)
    
    def locales(self):
        locales = []
        for bundle in self.bundles:
            locales.append(bundle.locale)
        return locales
    
    def has_locale(self, locale):
        return locale in self.locales()

    def slug(self, locale: str):
        return self.get("slug", locale, {})

    def slugs(self):
        return self.get_all("slug", {})

    def title(self, locale: str):
        return self.get("title", locale)
    
    def titles(self):
        return self.get_all("title", {})

    def get(self, key: str, locale: str, args):
        try:
            text, errs = self.bundle(locale).format(key, args)
            return text
        except:
            pass
        return None

    def get_all(self, key: str, args):
        vals = dict()
        for locale in self.locales():
            try:
                val = self.get(key, locale, args)
                if val != None:
                    vals[locale] = val
            except:
                pass
        
        return vals

    def get_delocalized(self, key: str):
        vals = []
        for locale in self.locales():
            try:
                val = self.get(key, locale)
                if val:
                    vals = vals + [val]
            except:
                pass

        return vals

class Section:
    def __init__(self, path: [str], page, template):
        self.path = path
        self.page = page
        self.pages = []
        self.sections = []
    
    def is_published(self, locale):
        return self.page.is_published(locale, default = False)
    
    def template(self, locale):
        default = "section"
        if is_index(self.path):
            default = "site"
        return self.page.template(locale, default)

    def ctx(self, site, locale: str, args: {}, require_published_section, require_published_pages):
        if locale not in self.locales():
            return None
        
        ctx = {"section": {}}
        published = False

        if self.page:
            page_ctx = self.page.ctx(None, self, locale, args, require_published_section, "section")
            if page_ctx:
                ctx["section"].update(page_ctx.pop("page"))
                
                published = parse_bool(ctx["section"]["published"])
        
        ctx["section"].update({
            "locale": locale,
            "href": self.href(locale),
            "published": published,
            "body": self.body(locale),
            "template": self.template(locale),
        })
        
        pages_ctx = []
        for page in self.pages:
            page_ctx = page.ctx(None, self, locale, args, require_published=False, default_template="page")
            if page_ctx:
                pages_ctx.append(page_ctx)
        
        if len(pages_ctx) >= 1:
            ctx["section"].update({"pages": pages_ctx})
        
        if len(ctx["section"].keys()) == 0 or (require_published_section and not published):
            return None
        
        return ctx
    
    def body(self, locale):
        if self.page:
            if locale in self.page.bodies:
                return self.page.bodies[locale]
        return None

    def locales(self):
        l = set()
        if self.page:
            l.update(self.page.locales())
        for page in self.pages:
            l.update(page.locales())
        return l

    def href(self, locale):
        return self.page.href(self, locale)

    def has_locale(self, locale):
        return locale in self.locales()

    def add_sections(self, sections: []):
        self.sections = self.sections + sections

    def add_pages(self, pages: [Page]):
        self.pages = self.pages + pages
    
    def has_pages(self):
        return len(self.pages) >= 1

class Site:
    def ctx(self, locale: str, args: {}, require_published_site=True, require_published_sections=False, require_published_pages=True):
        published = self.is_published(locale, default=False)
        ctx = {
            "site": {},
            "slugs": self.slugs,
        }

        page_ctx = self.page.ctx(self, self.root_section, locale, args, require_published_site, "site")
        if page_ctx:
            ctx["site"].update(page_ctx.pop("page"))
        
        ctx["site"].update({
            "locale": locale,
            "href": self.href(locale),
            "template": self.template(locale),
        })

        sections_ctx = []
        for section in self.sections:
            is_index_section = is_index(section.path)
            section_ctx = section.ctx(self, locale, args, False, require_published_pages)
            if section_ctx:
                if require_published_sections == False:
                    sections_ctx.append(section_ctx)
                elif "pages" in section_ctx["section"]:
                    if len(section_ctx["section"]["pages"]) >= 1:
                        sections_ctx.append(section_ctx)
            
        if len(sections_ctx) >= 1:
            ctx["site"].update({"sections": sections_ctx})
        elif require_published_site:
            return None

        return ctx
    
    def template(self, locale):
        return self.page.template(locale, "site")
    
    def is_published(self, locale, default):
        published = parse_bool(self.page.is_published(locale, default))
        return published
    
    def href(self, locale):
        return self.page.href(None, locale)

    def default_locale(self):
        if self.page:
            return self.page.default_locale
        else:
            return None

    def __init__(self, content_dir, build_dir, public_dir, templates_dir, styles_dir):
        self.root_section: Section
        self.sections = []
        self.page = None
        self.templates = jinja2.Environment()
        self.content_dir = content_dir
        self.build_dir = build_dir
        self.public_dir = public_dir
        self.templates_dir = templates_dir
        self.styles_dir = styles_dir
        self.locales = set()
        self.slugs = dict()
        self.paths = set()

        # TODO: make sure content_dir, templates_dir and styles_dir dont get removed when calling self.clean()
        #if content_dir in Path(build_dir).parts or content_dir in Path(public_dir).parts:
        #    raise Exception("content_dir must not be inside build_dir or inside public_dir")
        if len({content_dir, build_dir, public_dir, templates_dir, styles_dir}) < 5:
            raise Exception("content_dir, build_dir, public_dir, templates_dir and styles_dir must be distinct.")

        for root, dirs, files in os.walk(content_dir):
            section_path = Path(root)
            section_path = Path(*section_path.parts[1:])
            section_path_parts = section_path.parts
            section_path_end = str(*section_path_parts[len(section_path_parts)-1:])
            section_path = ["/"] + list(section_path_parts)
            section = Section(section_path, page = None, template = None)

            is_index_section: bool = is_index(section.path)
            if is_index_section:
                self.root_section = section

            for file in files:
                root_file = Path(root).joinpath(file)
                file_parts = file.split(".")
                file_start = file_parts[0]

                if file.endswith(".md"):
                    if len(file_parts) >= 3:
                        page_base = ".".join(file_parts[0:-2])
                        page_path = section_path
                        locale = file_parts[len(file_parts)-2:-1][0]
                        is_section_page = False
                        if is_index(page_base):
                            page_base = "/"
                            is_section_page = True
                        else:
                            page_path = page_path + [page_base]

                        file_handle = open(root_file, "r")
                        file_content = file_handle.read().lstrip()
                        file_handle.close()

                        frontmatter, body = extract_frontmatter(file_content)
                        if body == "":
                            body = None
                        else:
                            body = markdown(body, extensions=[TocExtension(baselevel=2)])
                        bodies = {locale: body}
                        
                        self.locales.update({locale})

                        bundle = FluentBundle.from_string(locale, frontmatter)
                        bundles = set({bundle})

                        slug = None
                        try:
                            slug_from_bundle, errs = bundle.format("slug", {})
                            if slug_from_bundle:
                                if len(slug_from_bundle) >= 1:
                                    slug = slug_from_bundle
                        except:
                            pass
                        
                        current_object = None

                        if is_index_section and is_section_page:
                            if self.page:
                                self.page.add_bundles(bundles)
                                self.page.add_bodies(bodies)
                            else:
                                self.page = Page(["/"], bundles, bodies)
                                section.page = self.page
                            current_object = self
                        elif is_section_page:
                            if section.page != None:
                                section.page.add_bundles(bundles)
                                section.page.add_bodies(bodies)
                            else:
                                section.page = Page(page_path, bundles, bodies)
                            current_object = section
                        else:
                            page_exists = False
                            for search_page in section.pages:
                                if search_page.path == page_path:
                                    page_exists = True
                                    search_page.add_bundles(bundles)
                                    search_page.add_bodies(bodies)
                                    current_object = search_page
                                    break
                            if not page_exists:
                                page = Page(page_path, bundles, bodies)
                                section.pages.append(page)
                                current_object = page
                        if slug and current_object:             
                            if slug in self.slugs:
                                raise Exception("Slug \"" + slug + "\" exists more than once.")
                            else:
                                self.slugs.update({slug: (locale, current_object)})

            self.sections.append(section)

    def clean(self):
        # TODO: make sure content_dir, templates_dir and styles_dir dont get removed when calling this function
        rmdir(self.build_dir)
        rmdir(self.public_dir)

    def build(self, require_published_site=True, require_published_sections=True, require_published_pages=True):
        build_templates_dir = str(Path(self.build_dir).joinpath(self.templates_dir))

        return_codes = []

        mkdir(self.build_dir)
        mkdir(build_templates_dir)
        mkdir(self.public_dir)

        dir_exists = {}

        # Convert templates from xmq.
        for root, dirs, files in os.walk(self.templates_dir):
            root = Path(root)
            dst_root_path = Path(self.build_dir).joinpath(root)

            dir_exists[dst_root_path] = False
            
            for file in files:
                if file.endswith(".xmq"):
                    if not dir_exists[dst_root_path]:
                        mkdir(dst_root_path)
                        dir_exists[dst_root_path] = True

                    src_path = str(root.joinpath(file))
                    dst_path = str(dst_root_path.joinpath(".".join(file.split(".")[:-1])))
                    try:
                        xmq_to_jinja2 = "xmq --pp --nodec " + src_path + " > " + dst_path
                        return_codes = return_codes + [exec(xmq_to_jinja2)]
                    except:
                        pass

        self.templates = jinja2.Environment(loader = jinja2.FileSystemLoader("build/templates/"), autoescape = True)

        for locale in self.locales:
            site_ctx = self.ctx(locale, {}, False, False, False)
            
            if site_ctx:
                if site_ctx["site"]["published"] == True or require_published_site == False:
                    site_html = self.templates.get_template(site_ctx["site"]["template"]).render(site_ctx)
                    site_href = site_ctx["site"]["href"]
                    site_dir = Path(site_href.lstrip("/"))
                    site_public_dir = Path(self.public_dir).joinpath(site_dir)
                    site_public_file = site_public_dir.joinpath("index.html")

                    if site_public_dir != self.public_dir:
                        mkdir(site_public_dir)
                    
                    print(site_public_file)
                    site_public_file_handle = open(site_public_file, "w")
                    site_public_file_handle.write(site_html)
                    site_public_file_handle.close()
                                
                sections_ctx = site_ctx["site"]["sections"]
                for section_ctx in sections_ctx:
                    section_ctx.update(site_ctx)
                    section_published = section_ctx["section"]["published"]
                    section_href = section_ctx["section"]["href"]
                    section_dir = Path(section_href.lstrip("/"))
                    section_public_dir = Path(self.public_dir).joinpath(section_href.lstrip("/"))
                    section_pages = section_ctx["section"]["pages"]
                    all_section_pages_published = False
                    section_has_unpublished_pages = False
                    section_has_published_pages = False
                    section_has_published_unslugged_pages = False
                    for page in section_ctx["section"]["pages"]:
                        if not page["page"]["published"]:
                            section_has_unpublished_pages = True
                            if not page["page"]["slug"]:
                                section_has_published_unslugged_pages = True
                        else:
                            section_has_published_pages = True
                    
                    

                    if section_published or not require_published_sections:
                        if section_has_published_unslugged_pages or section_published:
                            mkdir(section_public_dir)
                        if section_published and require_published_sections:
                            section_html = self.templates.get_template(section_ctx["section"]["template"]).render(section_ctx)
                            section_public_file = section_public_dir.joinpath("index.html")
                            section_public_file_handle = open(section_public_file, "w")
                            section_public_file_handle.write(section_html)
                            section_public_file_handle.close()

                    for page_ctx in section_ctx["section"]["pages"]:
                        if page_ctx["page"]["published"] and require_published_pages:
                            page_ctx.update(section_ctx)
                            page_ctx.update(site_ctx)

                            page_html = self.templates.get_template(page_ctx["page"]["template"]).render(page_ctx)
                            page_href = Path(page_ctx["page"]["href"].lstrip("/"))
                            page_slug = page_ctx["page"]["slug"]
                            page_public_dir = None
                            page_name = page_href.name
                            page_dir = page_href.parent
                            page_public_file = None
                            
                            if page_slug:
                                page_public_dir = Path(self.public_dir)
                                page_public_file = Path(page_public_dir).joinpath(page_slug)
                            else:
                                page_public_dir = section_public_dir
                                page_public_file = page_public_dir.joinpath(page_name)
                            if not is_index(page_dir):
                                mkdir(page_public_dir)
                                pass

                            page_public_file = str(page_public_file) + ".html"
                            page_public_file_handle = open(page_public_file, "w")
                            page_public_file_handle.write(page_html)
                            page_public_file_handle.close()

        for root, dirs, files in os.walk(self.public_dir):
            for file in files:
                file_path = str(Path(root).joinpath(file))
                if file.endswith(".html"):
                    # TODO: proper error logging/printing facing user
                    return_codes += [exec("tidy --tidy-mark no -i -w 80 -q -m " + file_path)]

        print("Site built. Happy fun located at \"" + self.public_dir + "\".")
        #return return_codes

site = Site(content_dir = "content/",build_dir = "build/",public_dir = "build/public/",templates_dir = "templates/",styles_dir = "sass/")

def main():
    # Available cmds:

    # Creates new project with the given name.
    # `new SITE_NAME`

    # Builds website into "build/public/".
    # `rebuild` (defaults to `--published`)
    # `rebuild --published`
    # `rebuild --unpublished`

    # Removes "build/".
    # `clean`

    # Uploads "build/public/" to server.
    # `sync ADDRESS`

    site = Site(
        content_dir = "content/",
        build_dir = "build/",
        public_dir = "build/public/",
        templates_dir = "templates/",
        styles_dir = "sass/",
    )
    

if __name__ == '__main__':
    main()