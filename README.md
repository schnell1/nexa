# nexa
Multilingual static site generator.

# Status
- soon alpha

# Features
Templates can be written in [XMQ](https://github.com/weetmuts/xmq#examples) embedding [Jinja2](https://jinja.palletsprojects.com/).  
Multilingual content can be written in [Markdown](https://itsfoss.com/markdown-guide/) with a frontmatter in [Fluent](https://projectfluent.org/).

# Usage
- `nexa new PROJECT_NAME` creates a directory with default directory structure and templates
- `nexa build` builds the project
- `nexa clean` removes "build/"
- `nexa serve` makes the website available at "http://localhost:3000" (uses `serve-html`)
- `nexa sync ADDRESS` uploads the website to the server (uses `rsync`)

# Requirements
- [XMQ](https://github.com/weetmuts/xmq)
- [Dart-Sass](https://sass-lang.com/dart-sass)
- [rsync](https://rsync.samba.org/)
- [Tidy](https://www.html-tidy.org/)
- [Python 3](https://www.python.org/):
- [fluent_compiler](https://github.com/django-ftl/fluent-compiler)
- [Jinja2](https://jinja.palletsprojects.com/)
- [Python-Markdown](https://python-markdown.github.io/)

## Optional
- [serve-html](https://gitlab.com/schnell1/serve-html) to run `nexa serve`

# License
Any at your option.
